package com.rybak;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroCollector;
import org.apache.avro.mapred.AvroMapper;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class SecondarySortMapper extends AvroMapper<GenericRecord, Pair<GenericRecord, GenericRecord>> {

    private Schema keySchema;

    @Override
    public void configure(JobConf jobConf) {
        keySchema = new Schema.Parser().parse(jobConf.get(SecondarySortDriver.MAP_KEY_SCHEMA));
    }

    @Override
    public void map(GenericRecord datum, AvroCollector<Pair<GenericRecord, GenericRecord>> collector, Reporter reporter)
            throws IOException {
        GenericRecord record = new GenericData.Record(keySchema);
        record.put("hotel_id", Long.parseLong(datum.get("hotel_id").toString()));
        record.put("id", Long.parseLong(datum.get("id").toString()));
        record.put("srch_ci", datum.get("srch_ci").toString());

        if (Integer.parseInt(datum.get("srch_adults_cnt").toString()) >= 2) {
            collector.collect(new Pair<GenericRecord, GenericRecord>(record, datum));
        }
    }
}
