package com.rybak;

import org.apache.avro.Schema;
import org.apache.avro.Schema.Type;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroJob;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.File;


public class SecondarySortDriver extends Configured implements Tool {

    public static final String MAP_KEY_SCHEMA = "secondarysort.key.schema";

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new SecondarySortDriver(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {

        //Get Input Arguments
		String input = args[0];// input file data.avro
		String output = args[1];// output path
		String inputSchemaFile = args[2];// output path
		String mapKeySchemaFile = args[3];// output path

        //Initialize MapReduce Job Conf
        JobConf conf = new JobConf(getConf(), SecondarySortDriver.class);
        conf.setJobName("Avro Secondary Sort ");

        //Set Input and Output Paths
        FileInputFormat.setInputPaths(conf, new Path(input));
        FileOutputFormat.setOutputPath(conf, new Path(output));

        //Get Input avro Schema
        Schema schema = new Schema.Parser().parse(new File(inputSchemaFile));
        //Get Mapper Key avro Schema
        Schema mapKeySchema = new Schema.Parser().parse(new File(mapKeySchemaFile));

        conf.set(MAP_KEY_SCHEMA, mapKeySchema.toString());

        AvroJob.setInputSchema(conf, schema);
        AvroJob.setMapOutputSchema(conf, Pair.getPairSchema(mapKeySchema, schema));
        AvroJob.setOutputSchema(conf, Pair.getPairSchema(Schema.create(Type.INT), schema));

        AvroJob.setMapperClass(conf, SecondarySortMapper.class);
        AvroJob.setReducerClass(conf, SecondarySortReducer.class);

        conf.setOutputValueGroupingComparator(GroupingKeyComparator.class);
        conf.setPartitionerClass(Part.class);

        JobClient.runJob(conf);
        return 0;
    }

    public static class Part implements Partitioner<AvroKey<GenericRecord>, AvroValue<GenericRecord>> {

        @Override
        public void configure(JobConf arg0) {
        }

        @Override
        public int getPartition(AvroKey<GenericRecord> key, AvroValue<GenericRecord> value, int numberOfPartitions) {
            return Math.abs(String.valueOf(key.datum().get("hotel_id")).hashCode() % numberOfPartitions);
        }
    }

}
