package com.rybak;

import org.apache.avro.generic.GenericRecord;
import org.apache.avro.mapred.AvroKeyComparator;
import org.apache.avro.mapred.AvroWrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GroupingKeyComparator extends AvroKeyComparator<GenericRecord> {

    public int compare(AvroWrapper<GenericRecord> x, AvroWrapper<GenericRecord> y) {
        int compare = String.valueOf(x.datum().get("hotel_id")).compareTo(String.valueOf(y.datum().get("hotel_id")));
        if (compare != 0) {
            return compare;
        }

        Date date1 = null;
        Date date2 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(x.datum().get("srch_ci").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            date2 = new SimpleDateFormat("yyyy-MM-dd").parse(y.datum().get("srch_ci").toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date1 != null;
        assert date2 != null;
        compare = date1.compareTo(date2);
        if (compare != 0) {
            return compare;
        }

        compare = String.valueOf(x.datum().get("id")).compareTo(String.valueOf(y.datum().get("id")));
        return compare;

    }
}
