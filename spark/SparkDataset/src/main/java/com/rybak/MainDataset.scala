package com.rybak

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, concat, lit}

object MainDataset {
  def main(args: Array[String]) {

    val inputAwardsPlayer = args(0)
    val inputMaster = args(1)
    val inputTeams = args(2)
    val inputScoring = args(3)
    val outputTop10parquet= args(4)
    val outputTop10JSON = args(5)
    val outputTop10CSV = args(6)

//    val inputAwardsPlayer = "src/main/resources/data/AwardsPlayers.csv"
//    val inputMaster = "src/main/resources/data/Master.csv"
//    val inputTeams = "src/main/resources/data/Teams.csv"
//    val inputScoring = "src/main/resources/data/Scoring.csv"
//    val outputTop10parquet = "outputTop10parquet"
//    val outputTop10JSON = "output10JSON"
//    val outputTop10CSV = "output10CSV"

    val spark = SparkSession
    .builder()
    .master("local")
    .appName("DataSet on Scala")
    .getOrCreate()
    import spark.implicits._

    val scoring = spark.read
      .option("header", "true")
      .csv(inputScoring)
      .select("playerID", "tmID", "G")
      .withColumnRenamed("G", "goals")

    val awards = spark.read
      .option("header", "true")
      .csv(inputAwardsPlayer)
      .select("playerID", "award")

    val master = spark.read
      .option("header", "true")
      .csv(inputMaster)
      .select("playerID", "firstName", "lastName")

    val teams = spark.read
      .option("header", "true")
      .csv(inputTeams)
      .select("tmID", "name")

    val scoringCast = scoring.select(
      scoring.col("playerID"),
      scoring.col("tmID"),
      scoring.col("goals").cast("integer"))

    val goalsNumber = scoringCast.map(s => {
      var goals = 0;
      if (s.get(2) != null && s.get(2) != "") {
        goals = s.getInt(2)
      }
      (s.getString(0), goals)
    }).groupByKey(_._1)
      .reduceGroups((a, b) => (a._1, a._2 + b._2)).map(_._2)
      .withColumnRenamed("_1", "playerId")
      .withColumnRenamed("_2", "goals")

    val awardsNumber = master
      .joinWith(awards, master.col("playerID") === awards.col("playerID"), "left")
      .map(s => {
        if (s._2 != null) {
          (s._1.getString(0), 1)
        } else {
          (s._1.getString(0), 0)
        }
      }).map(x => (x._1, x._2))
      .groupByKey(_._1)
      .reduceGroups((a, b) => (a._1, a._2 + b._2)).map(_._2)
      .withColumnRenamed("_1", "playerID")
      .withColumnRenamed("_2", "awards")

    val playerIdTeamNames = scoring.joinWith(teams, scoring.col("tmID") === teams.col("tmID"))
      .map(s => (s._1.getString(0), s._2.getString(1)))
      .distinct()
      .groupByKey(_._1)
      .reduceGroups((a, b) => (a._1, a._2 + ", " + b._2)).map(_._2)
      .withColumnRenamed("_1", "playerId")
      .withColumnRenamed("_2", "teams")

    val finalTable = master
      .join(awardsNumber, Seq("playerID"), "left_outer")
      .join(goalsNumber, Seq("playerID"), "left_outer")
      .join(playerIdTeamNames, Seq("playerID"), "left_outer")
      .withColumnRenamed("count", "awards")

    val finalTableFormatted = finalTable.select(
      concat(col("firstName"), lit(" "), col("lastName")).as("name"),
      finalTable.col("awards"),
      finalTable.col("goals"),
      finalTable.col("teams"))

    val top10toPrint = finalTableFormatted
      .orderBy(finalTableFormatted.col("goals").desc)
      .limit(10)

    top10toPrint.coalesce(1).write.option("header", "true").csv(outputTop10CSV)
    top10toPrint.coalesce(1).write.option("header", "true").json(outputTop10JSON)
    top10toPrint.coalesce(1).write.option("header", "true").parquet(outputTop10parquet)

    spark.stop()
  }
}
