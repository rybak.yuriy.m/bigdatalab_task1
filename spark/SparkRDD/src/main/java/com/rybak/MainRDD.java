package com.rybak;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class MainRDD {

    public static void main(String[] args) {

        SparkConf sparkConf = new SparkConf()
                .setAppName("Home work SPARK RDD")
                .setMaster("local");
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
        final  String del = " | ";
        String inputAwardsPlayer = args[0];
        String inputMaster = args[1];
        String inputTeams = args[2];
        String inputScoring = args[3];
        String outputTop10 = args[4];

//        String inputAwardsPlayer = "src/main/resources/data/AwardsPlayers.csv";
//        String inputMaster = "src/main/resources/data/Master.csv";
//        String inputTeams = "src/main/resources/data/Teams.csv";
//        String inputScoring = "src/main/resources/data/Scoring.csv";
//        String outputTop10 = "outputTop10";

        JavaRDD<String> awardsPlayers = sparkContext.textFile(inputAwardsPlayer);
        JavaRDD<String> master = sparkContext.textFile(inputMaster);
        JavaRDD<String> teams = sparkContext.textFile(inputTeams);
        JavaRDD<String> scoring = sparkContext.textFile(inputScoring);

        String teamsHeader = teams.first();
        String awardsHeader = awardsPlayers.first();
        String masterHeader = master.first();
        String scoringHeader = scoring.first();

        JavaRDD<String> filteredTeams = teams.filter(row -> !row.equals(teamsHeader));
        JavaRDD<String> filteredAwards = awardsPlayers.filter(row -> !row.equals(awardsHeader));
        JavaRDD<String> filteredMaster = master.filter(row -> !row.equals(masterHeader));
        JavaRDD<String> filteredScoring = scoring.filter(row -> !row.equals(scoringHeader));

        /**get pair: playerId | fullName - OK */
        JavaPairRDD<String, String> playerIdFullName = filteredMaster.mapToPair(line -> {
            String[] values = line.split(",");
            String playerId = values[0];
            if (("".equals(playerId))) {
                playerId = "unknown";
            }
            return new Tuple2<>(playerId, String.join(" ", values[3], values[4]));
        });

        /**get pair: playerId | goalSum */
        JavaPairRDD<String, Integer> playersIdGoals = filteredScoring.mapToPair(s -> {
            String[] values = s.split(",");
            String playerId = values[0];
            String goalsValue = "";

            if (values.length >= 8) {
                goalsValue = values[7];
            } else goalsValue = "";

            if (goalsValue.equals("")) {
                return new Tuple2<>(playerId, Integer.parseInt("0"));
            }
            return new Tuple2<>(playerId, Integer.parseInt(goalsValue));
        });
        JavaPairRDD<String, Integer> goalsSum = playersIdGoals.reduceByKey(Integer::sum);

        /**get pair: teamId | playerId */
        JavaPairRDD<String, String> playerIdTeamId = filteredScoring.mapToPair(s -> {
            String[] values = s.split(",");
            return new Tuple2<>(values[3], values[0]);
        }).distinct().sortByKey();

        /** get pair: teamId | teamName - OK*/
        JavaPairRDD<String, String> teamIdName = filteredTeams.mapToPair((PairFunction<String, String, String>) str -> {
            String[] data = str.split(",", -1);
            return new Tuple2<>(data[2], data[18]);
        });

        /**get pair: playerId | awardsNumber*/
        JavaPairRDD<String, Integer> playerIdAward = filteredAwards.mapToPair(s -> {
            String[] values = s.split(",");
            return new Tuple2<>(values[0], 1);
        });
        JavaPairRDD<String, Integer> playerIdAwardsNumber = playerIdAward.reduceByKey(Integer::sum);

        /**get joined table: playerId | fullName | awardsNum */
        JavaPairRDD<String, Tuple2<String, Optional<Integer>>> playerIdPlayerNameAwds =
                playerIdFullName
                        .leftOuterJoin(playerIdAwardsNumber);

        /**get joined table: playerId | fullName | awardsNum | goals */
        JavaPairRDD<String, Tuple2<Tuple2<String, Optional<Integer>>, Optional<Integer>>> playerIdPlayerNameAwdsGoals =
                playerIdPlayerNameAwds.leftOuterJoin(goalsSum);

        /**get pair: playerId | teams[] - OK. rows */
        JavaPairRDD<String, Tuple2<String, Optional<String>>> playerIdTeamIdTeamName =
                playerIdTeamId.leftOuterJoin(teamIdName).distinct();
        JavaPairRDD<String, Optional<String>> playerIdTeam =
                playerIdTeamIdTeamName.mapToPair(x -> new Tuple2<>(x._2._1, x._2._2));
        JavaPairRDD<String, Iterable<Optional<String>>> playerIdTeams =
                playerIdTeam.groupByKey();

        /**get joined table: playerId | fullName | awardsNum | goals | teams[] */
        JavaPairRDD<String,
                Tuple2<Tuple2
                        <Tuple2<String, Optional<Integer>>,
                                Optional<Integer>>,
                        Optional<Iterable<Optional<String>>>>> finalTable =
                playerIdPlayerNameAwdsGoals.leftOuterJoin(playerIdTeams);

        JavaPairRDD<Integer, Tuple2<Tuple2<String, Integer>, Iterable<Optional<String>>>>
                joinedWithoutPlayerID = finalTable.mapToPair(s ->
                (new Tuple2<>(s._2._1._2.orElse(0), new Tuple2<>(
                        new Tuple2<>(s._2._1._1._1, s._2._1._1._2.orElse(0)), s._2._2.orElse(new ArrayList<>())))))
                .sortByKey(false);

        JavaPairRDD<StringBuilder, StringBuilder> finalTableFormatted = joinedWithoutPlayerID.mapToPair(s -> {

            String name = s._2._1._1;
            String awards = s._2._1._2.toString();
            String goals = s._1.toString();
            List<String> teamsList = new ArrayList<>();
            s._2._2.forEach(r -> teamsList.add(r.orElse("")));
            StringBuilder teamsBuilder = new StringBuilder();
            for (int i = 0; i < teamsList.size(); i++) {
                teamsBuilder.append(teamsList.get(i));
                if (i != (teamsList.size() - 1)) {
                    teamsBuilder.append(", ");
                }
            }
            StringBuilder rowBuilder = new StringBuilder();
            rowBuilder.append(name).append(del).append(awards).append(del).append(goals).append(del);
            return new Tuple2<>(rowBuilder, teamsBuilder);
        });
        List<Tuple2<StringBuilder, StringBuilder>> top10Formatted = finalTableFormatted.take(10);

        StringBuilder headerBuilderNmAwrdGls = new StringBuilder();
        headerBuilderNmAwrdGls.append("name").append(del).append("awards").append(del).append("goals").append(del);
        StringBuilder headerBuilderTeams = new StringBuilder();
        headerBuilderTeams.append("teams");
        Tuple2<StringBuilder, StringBuilder> header = new Tuple2<>(headerBuilderNmAwrdGls,headerBuilderTeams);

        ArrayList<Tuple2<StringBuilder, StringBuilder>> resultedList = new ArrayList<>();
        resultedList.add(header);
        resultedList.addAll(top10Formatted);
        JavaRDD<Tuple2<StringBuilder, StringBuilder>> top10toPrint = sparkContext.parallelize(resultedList);
        top10toPrint.coalesce(1).map(x -> x.productIterator().mkString("")).saveAsTextFile(outputTop10);

        sparkContext.stop();
    }
}