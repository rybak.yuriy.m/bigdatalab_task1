# Commands for running spark applications

* SparkRdd application:
./spark-submit  --class com.rybak.MainRDD --master yarn --deploy-mode client /jars/SparkRDD-final.jar 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/AwardsPlayers.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Master.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Teams.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Scoring.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/RDDoutputPath

* SparkDataset application:
./spark-submit  --class com.rybak.MainDataset --master yarn --deploy-mode client /jars/SparkDataset-final.jar 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/AwardsPlayers.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Master.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Teams.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Scoring.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/DSoutputPath-parquet
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/DSoutputPath-json 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/DSoutputPath-csv


* SparkSQL application:
./spark-submit  --class com.rybak.MainSparkSQL --master yarn --deploy-mode client /jars/SparkSQL-final.jar
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/AwardsPlayers.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Master.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Teams.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/inputdata/Scoring.csv 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/SQLoutputPath-parquet
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/SQLoutputPath-json 
hdfs://sandbox.hortonworks.com:8020/spark-homeworks/SQLoutputPath-csv