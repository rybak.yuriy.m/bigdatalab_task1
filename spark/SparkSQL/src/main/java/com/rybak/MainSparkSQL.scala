package com.rybak

import org.apache.spark.sql.SparkSession


object MainSparkSQL {

  def main(args: Array[String]) {

    val spark = SparkSession
      .builder()
      .master("local")
      .appName("Task4-SparkSQL")
      .getOrCreate()

//    val inputAwardsPlayer = "src/main/resources/data/AwardsPlayers.csv"
//    val inputMaster = "src/main/resources/data/Master.csv"
//    val inputTeams = "src/main/resources/data/Teams.csv"
//    val inputScoring = "src/main/resources/data/Scoring.csv"
//    val outputTop10Parquet = "outputTop10Parquet"
//    val outputTop10JSON = "output10JSON"
//    val outputTop10CSV = "output10CSV"

        val inputAwardsPlayer = args(0)
        val inputMaster = args(1)
        val inputTeams =  args(2)
        val inputScoring =  args(3)
        val outputTop10Parquet = args(4)
        val outputTop10JSON =  args(5)
        val outputTop10CSV = args(6)
    import spark.implicits._
    spark.read
      .format("csv")
      .option("header", "true")
      .load(inputAwardsPlayer)
      .createOrReplaceTempView("awards")

    spark.read
      .format("csv")
      .option("header", "true")
      .load(inputMaster)
      .createOrReplaceTempView("master")

    spark.read
      .format("csv")
      .option("header", "true")
      .load(inputTeams)
      .createOrReplaceTempView("teams")

    spark.read
      .format("csv")
      .option("header", "true")
      .load(inputScoring)
      .createOrReplaceTempView("scoring")

    spark.sql("select playerID, COUNT(award) as awards from awards group by playerID")
      .createOrReplaceTempView("awards")

    spark.sql("select playerID, CONCAT(firstName,' ', lastName) AS full_name FROM master")
      .createOrReplaceTempView("name")

    spark.sql("select playerID, cast (SUM(G) as int) as goals FROM scoring group by playerID")
      .createOrReplaceTempView("goals")

    spark.sql("select distinct s.playerID, t.name as team from scoring s " +
      "join teams t on s.tmID=t.tmID")
      .createOrReplaceTempView("player_team")

    spark.sql("select distinct playerID, concat_ws(', ', collect_list(team)) as teams from player_team group by playerID")
      .createOrReplaceTempView("player_teams")

    spark.sql("select n.playerID, n.full_name, a.awards FROM name n " +
      "LEFT JOIN awards a on n.playerID=a.playerID")
      .createOrReplaceTempView("name_awards")

    spark.sql("select n.playerID, n.full_name, n.awards, g.goals FROM name_awards n " +
      "LEFT JOIN goals g on n.playerID=g.playerID")
      .createOrReplaceTempView("name_awards_goals")

    spark.sql("select distinct n.full_name, cast(n.awards as int), n.goals, p.teams FROM name_awards_goals n " +
    "LEFT JOIN player_teams p on n.playerID=p.playerID")
    .createOrReplaceTempView("final_table")

    val top10 = spark.sql("select * from final_table order by goals DESC limit 10 ")
      .na.fill(0.0, Seq("awards"))

    top10.map(s => {
      val name = s.getAs[String]("full_name")
      val awards = s.getAs[Int]("awards")
      val goals = s.getAs[Int]("goals")
      val teams = s.getAs[String]("teams")
     (name, awards, goals, teams)
    }).withColumnRenamed("_1", "name")
      .withColumnRenamed("_2", "awards")
      .withColumnRenamed("_3", "goals")
      .withColumnRenamed("_4", "teams")

      top10.coalesce(1).write.option("header", "true").json(outputTop10JSON)
      top10.coalesce(1).write.option("header", "true").parquet(outputTop10Parquet)
      top10.coalesce(1).write.option("header", "true").csv(outputTop10CSV)

    spark.stop()
  }

}
