package com.rybak.consumer

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, _}

import scala.io.Source

object MainConsumer extends App {

  private final val BROKER_PORT = "localhost:9092"
  private final val TOPIC = "topic1"
  private final val CENSORED_DICTIONARY = "src/main/resources/censored.txt"
  private final val WINDOW_DURATION = 10

  override def main(args: Array[String]): Unit = {

    val speakerField = "speaker"
    val timeField = "time"
    val wordField = "word"
    val censored_replace = "*CENSORED!*"

    val ss = SparkSession.builder()
      .appName("StreamingExample")
      .master("local")
      .getOrCreate()

    ss.sparkContext.setLogLevel("ERROR")
    import ss.implicits._
    val inputValue = ss
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", BROKER_PORT)
      .option("subscribe", TOPIC)
      .load()

    val message = inputValue.selectExpr("cast(value as string) as value")
      .withColumn("value", regexp_replace(col("value"), "/\"+", ""))
      .withColumn("value", regexp_replace(col("value"), "}", ""))
      .as[String].map(s => {
      val parsedInput = s.split(",")
      var speaker = ""
      var time = ""
      var word = ""
      for (record <- parsedInput) {
        if (record.contains(speakerField)) {
          speaker = record.split(":")(1).replaceAll("\"", "").trim
        }
        if (record.contains(timeField)) {
          time = record.split(":", 2)(1).replaceAll("\"", "").trim
        }
        if (record.contains(wordField)) {
          word = record.split(":")(1).replaceAll("\"", "").trim
        }
      }
      (speaker, time, word)
    })
      .withColumnRenamed("_1", "speaker")
      .withColumnRenamed("_2", "time")
      .withColumnRenamed("_3", "word")
      .as[Message]

    val censoredMessage = message.map(m => {
      if (isContainCensoredWord(m, CENSORED_DICTIONARY)) {
        (m.speaker, m.time, censored_replace)
      } else {
        (m.speaker, m.time, m.word)
      }
    })
      .withColumnRenamed("_1", "speaker")
      .withColumnRenamed("_2", "time")
      .withColumnRenamed("_3", "word").as[Message]

    val withEventTime = censoredMessage.select(
      col("speaker"),
      col("time").cast("timestamp").alias("time"),
      col("word"))

    val censoredCount = censoredMessage.filter(m => m.word == censored_replace).groupBy(col("speaker"))
      .count()
      .withColumnRenamed("count", "censored_count")
      .as[CensoredCount]

    withEventTime
      .select(col("time"), col("speaker"), col("word"))
      .groupBy(window(col("time"), WINDOW_DURATION + " minutes", WINDOW_DURATION + " minutes") as "window",
        col("speaker"))
      .agg(collect_list(col("word")).as("words"))
      .select(col("window"), col("words"), col("speaker"))
      .writeStream
      .option("truncate", "false")
      .format("console")
      .outputMode("update")
      .start()

    censoredCount
      .select(col("speaker"), col("censored_count"))
      .writeStream
      .option("truncate", "false")
      .format("console")
      .outputMode("update")
      .start()

    ss.streams.awaitAnyTermination()
  }

  def isContainCensoredWord(message: Message, censoredPath: String): Boolean = {
    val censoredWordsList = Source.fromFile(censoredPath).getLines.toList
    var result = false
    for (i <- 0 until censoredWordsList.length) {
      if (message.word.contains(censoredWordsList(i))) {
        result = true
      }
    }
    result
  }

  case class Message(speaker: String, time: String, word: String)
  case class CensoredCount(speaker: String, censored_count: Long)

}