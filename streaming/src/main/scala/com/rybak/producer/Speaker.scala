package com.rybak.producer

import java.time.LocalDateTime

import scala.collection.mutable


class Speaker(name: String, time: LocalDateTime) {

  private val timeStack = mutable.Stack[LocalDateTime](time)

  def getLastTime(): LocalDateTime = {
    timeStack.pop()
  }

  def updateLastTime(t: LocalDateTime): Unit = {
    timeStack.push(t)
  }

  def getName(): String = {
    name
  }

}

