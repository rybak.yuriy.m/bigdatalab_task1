package com.rybak.producer

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import scala.collection.mutable
import scala.io.Source
import scala.util.Random

class MessageGenerator(speakers: Array[Speaker], dictionaryPath: String) {

  private val TIME_TO_ADD = 60
  private val timeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S")

  private val speaker = speakers(Random.nextInt(speakers.length))
  private val wordsList = Source.fromFile(dictionaryPath).getLines.toList
  private val word = wordsList(Random.nextInt(wordsList.length-1))
  private val lastSpeakerTime = speaker.getLastTime()
  private val talkSpeakerTime = lastSpeakerTime.plusSeconds(Random.nextInt(TIME_TO_ADD))
  speaker.updateLastTime(talkSpeakerTime)

  private val timeFormatted = talkSpeakerTime.format(timeFormat)

  def getMessage: Message = {
    Message(speaker.getName(), timeFormatted, word)
  }

}

case class Message(speakerName: String, time: String, word: String) {
  override def toString: String = {
    "\"speaker\"" + ":" + " \"" + speakerName + "\",\n" +
    "\"time\""    + ":" + " \"" + time        + "\",\n" +
    "\"word\""    + ":" + " \"" + word        + "\""
  }


}