package com.rybak.producer

import java.time.LocalDateTime
import java.util.Properties

import com.rybak.producer.MainProducer.args
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

object MainProducer extends App {
//  private final val BROKER_PORT = "localhost:9092"
//  private final val TOPIC = "topic1"
//  private final val DICTIONARY_PATH = "src/main/resources/dictionary.txt"
//  private final val SPEAKERS_INPUT = "Obama, Trump, Mandela"

  override def main(args: Array[String]): Unit = {
    val BROKER_PORT = args(0)
    val TOPIC = args(1)
    val DICTIONARY_PATH = args(2)
    val SPEAKERS_INPUT = args(3)
    val startTime = LocalDateTime.now()
    val prop = new Properties()
    prop.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BROKER_PORT)
    prop.put("acks", "all")
    prop.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    prop.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](prop)
    val speakers = SPEAKERS_INPUT.split(',').map(s => new Speaker(name = s.trim, time = startTime))

    var i = 0
    while (i < 50) {
      sendMessage(speakers, DICTIONARY_PATH, producer, TOPIC, i)
      i = i + 1
    }
    producer.close()
  }

  def sendMessage(speakers: Array[Speaker], path: String,
                  producer: KafkaProducer[String, String], topic: String, key: Int): Unit = {
    val value = new MessageGenerator(speakers = speakers, dictionaryPath = path).getMessage.toString
    producer.send(new ProducerRecord[String, String](topic, key.toString, value))
  }

}

